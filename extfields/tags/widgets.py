#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
"""
WIDE IO
Revision FILE: $Id: widgets.py 841 2009-08-23 13:01:52Z tranx $


#############################################################################################################
copyright $Copyright$
@version $Revision: 841 $
@lastrevision $Date: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
@modifiedby $LastChangedBy: tranx $
@lastmodified $LastChangedDate: 2009-08-23 22:01:52 +0900 (Sun, 23 Aug 2009) $
#############################################################################################################
"""
# create widgets related to jquery component for essential displays ...
import datetime
import re
import uuid

from django.db.models.fields import NOT_PROVIDED
from django.forms.widgets import Widget
from django.template import Context, loader
from django.utils.safestring import mark_safe
import  settings


def getuuid():
    return str(uuid.uuid1())


class KeywordWidget(Widget):

    class Media:
        #js = [ "js/editable.js" ]
        #css = {'all': ["css/jquery.wysiwyg.css"]}
        pass

    def __init__(
            self,
            attrs={},
            multiple=True,
            model=None,
            humanid=None,
            *args,
            **kwargs):
        self.attrs = attrs
        self.model = model
        self.humanid = (
            humanid if humanid is not None else (
                model.WIDEIO_Meta.search_enabled if hasattr(
                    model,
                    "WIDEIO_Meta") and hasattr(
                    model.WIDEIO_Meta,
                    "search_enabled") else "name"))
        self.multiple = multiple

    def value_from_datadict(self, data, files, name):
        from extfields.models import Keyword
        print "Value_from_datadict BEGIN:", data, name
        val = []
        try:
            kwlist = map(lambda w: w.strip(), data[name].split(","))
            print kwlist, type(kwlist)
            for kw in kwlist:
                print 'kw ' + kw
                r = Keyword.objects.filter(name=kw)
                if r.count() > 0:
                    val.append(r[0].id)
                else:
                    kwo = Keyword()
                    kwo.name = kw
                    kwo.save()
                    val.append(kwo.id)
            print "Value_from_datadict END:", val
        except Exception as e:
            print e
        return val

    def render(self, name, value, attrs=None):
        from extfields.models import Keyword
        widgettemplate = loader.get_template('extfields/tag.html')
        object_class = self.model

        if (value is None) or not value:
            value = ""
        else:
            print "render keyword >> ", value
            # < handling query not supported by database case
            if (type(value[0]) not in [str, unicode]):
                # print "Value", value, dir(value[0])
                # print value[0].name ## tags
                # print value[0].m2m_field_name() ##algorithm
                # print value[0].m2m_reverse_field_name() ##keyword
                #print (type(value))
                kwquery = {value[0].m2m_field_name(): value[1]}
                relmodel = getattr(
                    value[0].model.objects.get(
                        id=value[1]),
                    value[0].name).through
                value = relmodel.objects.filter(**kwquery)
                # if (value is None):
                #    value=[]
                # print relmodel.objects.all().count()
                value = map(
                    lambda kw: relmodel.objects.get(
                        pk=kw.id).name_id,
                    value)
                # print "KEYWORDS",value
            value = map(
                lambda kw: unicode(
                    Keyword.objects.get(
                        pk=kw)), filter(
                    lambda x: type(x) in [
                        str, unicode], value))
            value = ",".join(value)
            # if (type(value)!=tuple):
            #     value=",".join([x.id for x in value])
            # else:
            #     q={value[0].model.__name__.lower()+"_id":value[1]}
            #     q={}
            #     value=",".join([getattr(x,value[0].rel.to.__name__.lower) for x in value[0].rel.through.objects.filter(**q)])
            #    print "CONTEXT ", Context({'name':name,'value':value, 'widgetuuid': getuuid(), 'object_class':object_class,
            #                                        'objectlist':self.choices, 'multiple':self.multiple,
            #                                        'humanid':self.humanid, 'MEDIA_URL': settings.MEDIA_URL})

        html_content = widgettemplate.render(
            Context(
                {
                    'name': name,
                    'value': value,
                    'widgetuuid': uuid.uuid1(),
                    'object_class': object_class,
                    'objectlist': self.choices,
                    'multiple': self.multiple,
                    'humanid': self.humanid,
                    'MEDIA_URL': settings.MEDIA_URL}))
        return mark_safe(html_content)
