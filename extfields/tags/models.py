# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import uuid

from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from django.core.urlresolvers import reverse

from django.views.decorators.csrf import csrf_exempt

import wioframework.fields as models

from wioframework import decorators as dec
from wioframework import validators
from wioframework.amodels import wideiomodel, wideio_owned, wideio_timestamped_autoexpiring, get_dependent_models, wideio_timestamped, wideio_action, wideio_publishable, wideio_followable


import settings

MODELS = []


@wideio_owned("useraccount")
@wideiomodel
class RelFollowKeyword(models.Model):
    keyword = models.ForeignKey('Keyword')

    @classmethod
    def get_icon(cls):
        from extfields.tags.models import Keyword
        return Keyword.get_icon()

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        MOCKS={
            'default': [
                {
                 'keyword': 'keyword-0000'
                }
            ]
        }


@wideio_publishable()
@wideio_followable('RelFollowKeyword')
@wideio_timestamped
@wideiomodel
class Keyword(models.Model):

    """
    Not modifiable, these belong to everyone.

    IDEA ?
    ## example : CS:DEEP LEARNING
    #domain = models.ForeignKey(Keyword) (nature of the relation "is a" / "talks about.." ....)

    IDEA ?
    HYPERKW / HYPOKW / SYNOKW ? ALT LANGUAGE ?
    """

    name = models.CharField(
        verbose_name="Keyword",
        max_length=64,
        db_validators=[
            validators.NameValidator])
    use_count = models.IntegerField(default=0)
    #use_count_history=ListField(models.IntegerField(), null=True, blank=True)

    def __unicode__(self):
        return unicode(self.name) or u''

    def on_add(self, request):
        self.use_count = 0

    def get_all_references(self):
        return []

    #~ def on_update(self, request):
        #~ self.use_count++##### ONLY IF NOT ALREADY THERE BEFORE

    class WIDEIO_Meta:
        NO_DRAFT = True
        form_exclude = ['use_count_history']
        sort_enabled = ['name', 'use_count']
        search_enabled = 'name'
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user.is_superuser,
            lambda r: (
                r.user is not None))
        MOCKS={
            'default': [
                {
                 'name': 'OpenScience',
                 'count': 1234
                }
            ]
        }

MODELS.append(Keyword)
MODELS += get_dependent_models(Keyword)
